import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ilerazy implements IleRazyLitera {

    public static void main(String[] arg) {
        ilerazy i = new ilerazy();
        int ile = i.ilerazy("ass asas asas asas", 's');
        System.out.println("Litera wystapiła: " + ile + " razy.");
    }


    public boolean czyLiczba(Character c) {
        if (Character.isDigit(c)) {
            return true;
        }
        return false;
    }

    //    @Override
    public boolean czyString(String s) {
        boolean isString = true;
        for (int i = 0; i < s.length(); i++) {
            if (czyLiczba(s.charAt(i))) {
                isString = false;
            }
        }
        return isString;
    }

    public boolean czyChar(Character c) {
        if (czyLiczba(c)) {
            return false;
        } else {
            return true;
        }
    }


    public boolean czyDanePoprawne(String s, Character c) {
        if (czyString(s) && (czyChar(c))) {
            return true;
        } else if (!czyString(s)) {
            System.out.println("String nie poprawny");
            return false;
        } else if (!czyChar(c)) {
            System.out.println("Znak niepoprawny");
            return false;
        } else return false;
    }

    @Override
    public int ilerazy(String s, Character c) {
        int licznik = 0;
        if (czyDanePoprawne(s, c)) {
            for (int i = 0; i < s.length(); i++) {
                if (Character.compare(s.charAt(i), c) == 0) {
                    licznik++;
                }
            }

        } else {
            System.out.println("twoje dane są niepoprawne");

        }
        return licznik;
    }
}
