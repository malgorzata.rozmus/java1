public class IleRazyLitera2 {


    public static void main(String[] arg) {
        ilerazy i = new ilerazy();
        int ile = i.ilerazy("ass asssssssssssssssas asas asas", 's');
        System.out.println("Litera wystapiła: " + ile + " razy.");
    }


    public boolean czyLiczba(Character c) {
        if (Character.isDigit(c)) {
            return true;
        }
        return false;
    }

    //    @Override
    //string nie moze miec zadnej cyfry
    public boolean czyString(String s, liczbaCondition lc) {
        boolean isString = true;
        for (int i = 0; i < s.length(); i++) {
            if (lc.liczbaConditionMetod(s.charAt(i))) {
                isString = false;
            }
        }
        return isString;
    }

    // znak nie moze byc cyfra
    public boolean czyChar(Character c, liczbaCondition lc) {
        if (lc.liczbaConditionMetod(c)) {
            return false;
        } else {
            return true;
        }
    }


    public boolean czyDanePoprawne(String s, Character c) {

        if (czyString(s, new liczbaCondition() {
            @Override
            public boolean liczbaConditionMetod(Character c) {
               return Character.isDigit(c);
            }
        }) && (czyChar(c, new liczbaCondition() {
            @Override
            public boolean liczbaConditionMetod(Character c) {
                return Character.isDigit(c);
            }
        }))) {
            return true;
        } else if (!czyString(s, new liczbaCondition() {
            @Override
            public boolean liczbaConditionMetod(Character c) {
                return Character.isDigit(c);
            }
        })) {
            System.out.println("String nie poprawny");
            return false;
        } else if (!czyChar(c, new liczbaCondition() {
            @Override
            public boolean liczbaConditionMetod(Character c) {
                return Character.isDigit(c);
            }
        })) {
            System.out.println("Znak niepoprawny");
            return false;
        } else return false;
    }


    public int ilerazy(String s, Character c) {
        int licznik = 0;
        if (czyDanePoprawne(s, c)) {
            for (int i = 0; i < s.length(); i++) {
                if (Character.compare(s.charAt(i), c) == 0) {
                    licznik++;
                }
            }

        } else {
            System.out.println("twoje dane są niepoprawne");

        }
        return licznik;
    }
}

interface liczbaCondition{
    public boolean liczbaConditionMetod(Character c);
}
